/* \author Aaron Brown */
// Quiz on implementing simple RANSAC line fitting

#include <chrono>
#include <string>
#include "kdtree.h"

template<typename PointT>
class EuclideanCluster
{
public:
	//constructor
	EuclideanCluster();

	//deconstructor
	~EuclideanCluster();

	void SetClusterTolerance(float inputClusterTolerance){clusterTolerance=inputClusterTolerance;}

	float GetClusterTolerance(){return clusterTolerance;}

	void SetMinClusterSize(int inputMinClusterSize){minSize = inputMinClusterSize;}

	int GetMinClusterSize(){return minSize;}

	void SetMaxClusterSize(int inputMaxClusterSize){maxSize = inputMaxClusterSize;}

	int GetMaxClusterSize(){return maxSize;}

	void SetKdTree(KdTree* inputTree){tree = inputTree;}

	KdTree* GetKdTree(){return tree;}

	std::vector<std::vector<int>> Extract(typename pcl::PointCloud<PointT>::Ptr inputCloud);

private:
	KdTree* tree;
	float clusterTolerance;
	int minSize;
	int maxSize;
	void ClusterHelper(typename pcl::PointCloud<PointT>::Ptr inputCloud, int ind, std::vector<int>& cluster, std::vector<bool>& processed);
};
