#include "euclideanCluster.h"

//constructor
template<typename PointT>
EuclideanCluster<PointT>::EuclideanCluster():tree(new KdTree), clusterTolerance(100),minSize(1), maxSize(1000){}

//deconstructor
template<typename PointT>
EuclideanCluster<PointT>::~EuclideanCluster(){}

template<typename PointT>
void  EuclideanCluster<PointT>::ClusterHelper(typename pcl::PointCloud<PointT>::Ptr inputCloud, int ind, std::vector<int>& cluster, std::vector<bool>& processed)
{
	{
		processed[ind] = true;
		cluster.push_back(ind);
		std::vector<float> point {inputCloud->points[ind].x,inputCloud->points[ind].y,inputCloud->points[ind].z};
		std::vector<int> nearest = tree->search(point, clusterTolerance);

		for (int id : nearest)
		{
			if (!processed[id])
				ClusterHelper(inputCloud, id, cluster, processed);
		}
	}
}

template<typename PointT>
std::vector<std::vector<int>> EuclideanCluster<PointT>::Extract(typename pcl::PointCloud<PointT>::Ptr inputCloud)
{
	std::vector<std::vector<int>> clusters, trimmedClusters;
	std::vector<bool> processed(inputCloud->points.size(), false);

	for (int i=0; i<inputCloud->points.size();i++)
	{
		if (!processed[i])
			{
				std::vector<int> cluster;
				ClusterHelper(inputCloud, i, cluster, processed);
				if (cluster.size() >= minSize && cluster.size()<= maxSize)
				clusters.push_back(cluster);
			}
	}
	return clusters;
}
